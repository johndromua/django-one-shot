from django.shortcuts import render, redirect, get_object_or_404


from todos.models import TodoList
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {"view_list": todolist}
    return render(request, "todolist/list.html", context)


def todo_list_detail(request, id):
    todolist = TodoList.objects.get(id=id)
    context = {"todolist": todolist}

    return render(request, "todolist/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todolist/create.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {"form": form}
    return render(request, "todolist/update.html", context)


def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todolist/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todoitem/create.html", context)
